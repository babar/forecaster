## Weather Forecaster (built for PlayOn Demo)

### How to setup the project locally

* Make sure you've Ruby (Preferably > v2.3.0) and `bundler` gem installed.
* Clone the repository
* Enter the directory and run `bundle install`
* Run `rails s`
* Visit `http://localhost:3000`
* You should see the homepage.

### How to deploy the app to production

* Make sure you've NginX, Ruby (Preferably > v2.3.0) and `bundler` gem installed on server.
* Update `config/deploy/production.rb` with your server details.
* If you're not using RVM on server, comment out the `rvm` line in `Capfile`
* Run `cap production deploy:check`, it'll report if things aren't in order. Fix them.
* Run `cap production deploy`, it'll deploy and run Puma on server.
* Log into server and link/copy `current/config/nginx.conf` into your NginX config.
* Restart Nginx and Visit your URL
* You should see the homepage.
