describe 'Checking forecast', type: :feature do

  scenario 'Can fill in location and get weather info' do
    visit '/'
    within('#forecast') {fill_in 's', with: 'Dhaka, BD'}
    click_button 'Check now'

    expect(page).to have_content('5 days forecast for Dhaka')
  end

  scenario 'Cannot get forecast without filling location' do
    visit '/'

    expect{click_button 'Check now'}.to raise_error(ActionController::RoutingError)
  end
end