describe 'Static Pages', type: :feature  do
  scenario 'Home page should be reachable' do
    visit '/'
    expect(page.status_code).to eq(200)
  end
end