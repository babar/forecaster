# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'forecaster'
set :repo_url, 'git@gitlab.com:babar/forecaster.git'

set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

set :log_level, :info

set :erb_files, fetch(:erb_files, []).push('config/nginx.conf.erb')