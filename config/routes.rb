Rails.application.routes.draw do
  root 'pages#home'
  # get 'forecast', to: 'forecasts#index'
  resources :forecasts, only: [:index, :show], param: :date
end
