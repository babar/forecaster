set :user, 'babar'
set :deploy_to, -> {"/home/#{fetch(:user)}/Sites/#{fetch(:application)}"}
set :host, 'forecaster.ibabar.com'
server 'Ruby', user: fetch(:user), roles: %w{app db web}