module ApplicationHelper
  def humanize_date(date)
    date = DateTime.parse(date)
    date.strftime("%A #{date.day.ordinalize}, %B")
  end
end
