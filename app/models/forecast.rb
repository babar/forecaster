class Forecast
  # Todo: Improve cache storage API

  def initialize(cache_storage = {})
    @session = cache_storage
  end

  def self.find(loc, date = nil)
    new.find(loc, date)
  end

  def find(loc, date = nil)
    data = get_data(loc)

    if date
      data[:forecast][date].merge(city: data[:forecast][:city])
    else
      data[:forecast]
    end
  end

  private
    def get_data(loc)
      if session[:weather] && session[:weather][:loc] == loc
        session[:weather]
      else
        sort_n_cache_date(loc, call_data_api(loc))
      end
    end

    def call_data_api(loc)
      HTTParty.get(
          'http://api.openweathermap.org/data/2.5/forecast',
          {query: {q: loc, units: 'metric', appid: api_key}}
      )
    end

    def sort_n_cache_date(loc, data)
      session[:weather] = {}
      session[:weather][:forecast] = {city: data['city']['name']}
      session[:weather][:loc] = loc
      session[:weather][:last_update] = Time.now #to Debug cache


      data['list'].each do |item|
        day = item['dt_txt'].split.first

        if session[:weather][:forecast][day].nil?
          session[:weather][:forecast][day] = filter_weather_data(item)
        end
      end

      session[:weather]
    end

    def filter_weather_data(data)
      {
          temp: data['main']['temp'],
          humidity: data['main']['humidity'],
          weather: data['weather'][0]['description'],
          icon: data['weather'][0]['icon'],
          wind: data['wind']['speed']
      }
    end

    def session
      @session ||= {}
    end

    def api_key
      OpenWeather::API_KEY
    end
end