class ForecastsController < ApplicationController
  before_action :enforce_location_param

  def index
    # @forecasts = Forecast.find(@city)
    @forecasts = Forecast.new(session).find(params[:s])
    @city = @forecasts[:city]
    @forecasts = @forecasts.except(:city)
  end

  def show
    @date = params[:date]
    @forecast = Forecast.new(session).find(params[:s], @date)
    @city = @forecast[:city]
  end

  private
    def enforce_location_param
      raise ActionController::RoutingError.new('Invalid Location') unless params[:s] && params[:s].size > 0
    end
end